## Demo Application

A demo version of this application is running at [https://morning-coast-21876.herokuapp.com/](https://morning-coast-21876.herokuapp.com/)

## Development Setup

There are a few prerequisites that need to be installed. Please contact me if you run into any issues and I will help to get you going.

### Install and start PostgreSQL
```
brew install postgresql
pg_ctl -D /usr/local/var/postgres start
```


### Install Ruby
```
brew install rbenv
rbenv install 2.5.1
```

NOTE: If you already have a version of Ruby installed on your system, you may want to try modifying the 'ruby' version in the Gemfile. Unless it's a really old Ruby, everything should work fine.

### Install Node and Yarn
```
brew install node
brew install yarn
```


### Install dependencies
```
cd [$PROJECT_ROOT]
gem install rails foreman bundler
bundle install

cd client
yarn install
```

### Setup the database
```
rake db:create
rake db:migrate
rake db:seed
```

### Start the server and frontend
```
cd [$PROJECT_ROOT]
foreman start
```
