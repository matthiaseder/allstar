Rails.application.routes.draw do

  resources :organizations, only: [:show]
  resources :activities
  resources :interests, only: [:create, :show] do
    collection do
      delete '', to: 'interests#destroy'
    end
  end
  resources :teams, only: [:index, :show]
  resources :sports, only: [:index, :show]
  resources :users, only: [:show, :update], path: :profiles do 
    member do
      get 'activities', to: 'activities#index'
    end
    collection do
      get 'me', to: 'users#index'
    end
  end

end
