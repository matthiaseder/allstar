class CreateUsers < ActiveRecord::Migration[5.2]
  def change
    create_table :users do |t|
      t.string :firstname
      t.string :lastname
      t.string :email
      t.string :avatar
      t.integer :height
      t.integer :weight
      t.boolean :private
      t.timestamps
    end
    add_index :users, :email
  end
end
