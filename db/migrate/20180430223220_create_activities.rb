class CreateActivities < ActiveRecord::Migration[5.2]
  def change
    create_table :activities do |t|
      t.integer :user_id
      t.integer :sport_id
      t.integer :team_id
      t.datetime :started_at
      t.integer :duration

      t.timestamps
    end
    add_index :activities, :user_id
    add_index :activities, :sport_id
    add_index :activities, :team_id
    add_index :activities, :started_at
  end
end
