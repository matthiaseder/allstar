class CreateInterests < ActiveRecord::Migration[5.2]
  def change
    create_table :interests do |t|
      t.integer :user_id
      t.integer :sport_id

      t.timestamps
    end
    add_index :interests, :user_id
    add_index :interests, :sport_id
  end
end
