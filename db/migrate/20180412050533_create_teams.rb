class CreateTeams < ActiveRecord::Migration[5.2]
  def change
    create_table :teams do |t|
      t.string :name
      t.string :avatar
      t.integer :sport_id
      t.integer :organization_id
      t.timestamps
    end
    add_index :teams, :sport_id
    add_index :teams, :organization_id
  end
end
