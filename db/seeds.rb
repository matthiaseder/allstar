# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

[
  { name: 'Badminton', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/badminton.svg' },
  { name: 'Baseball', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/baseball.svg' },
  { name: 'Basketball', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/basketball.svg' },
  { name: 'Bowling', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/bowling.svg' },
  { name: 'Cycling', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/cycling.svg' },
  { name: 'Football', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/football.svg' },
  { name: 'Golf', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/golf.svg' },
  { name: 'Hockey', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/hockey.svg' },
  { name: 'Soccer', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/soccer.svg' },
  { name: 'Table Tennis', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/tabletennis.svg' },
  { name: 'Tennis', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/tennis.svg' },
  { name: 'Ultimate Frisbee', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/default.svg' },
  { name: 'Volleyball', avatar: 'https://s3-us-west-1.amazonaws.com/allstar-icons/volleyball.svg' },
].each do |p|
  Sport.create(p)
end

soccer = Sport.find_by(name: 'Soccer')
cycling = Sport.find_by(name: 'Cycling')
ultimate = Sport.find_by(name: 'Ultimate Frisbee')

pnr = Organization.create(name: 'Folsom Parks & Recreation')

folsom_fc = soccer.teams.create(name: 'Folsom FC') do |t|
  t.organization = pnr
  t.avatar = 'https://s3-us-west-1.amazonaws.com/allstar-icons/teams/folsom-fc.png'
end

full_cycle = cycling.teams.create(name: 'Full Cycle') do |t|
  t.organization = pnr
  t.avatar = 'https://s3-us-west-1.amazonaws.com/allstar-icons/teams/full-cycle.png'
end

bruins = ultimate.teams.create(name: 'Bear River Bruins') do |t|
  t.organization = pnr
  t.avatar = 'https://s3-us-west-1.amazonaws.com/allstar-icons/teams/bear-river-bruins.png'
end

matthias = User.create(firstname: 'Matthias', lastname: 'Eder') do |u|
  u.email = 'nodeful@gmail.com'
  u.avatar = 'https://s3-us-west-1.amazonaws.com/allstar-icons/users/matthiaseder.jpeg'
  u.height = 68
  u.weight = 194
  u.private = false
end

matthias.interests.create(sport: soccer)
matthias.interests.create(sport: cycling)
matthias.interests.create(sport: ultimate)
matthias.memberships.create(team: folsom_fc)
matthias.memberships.create(team: bruins)

matthias.activities.create do |a|
  a.team = folsom_fc
  a.sport = folsom_fc.sport
  a.started_at = 10.days.ago
  a.duration = 46
end

matthias.activities.create do |a|
  a.team = folsom_fc
  a.sport = folsom_fc.sport
  a.started_at = 4.days.ago
  a.duration = 92
end

matthias.activities.create do |a|
  a.team = folsom_fc
  a.sport = folsom_fc.sport
  a.started_at = 2.days.ago
  a.duration = 68
end

matthias.activities.create do |a|
  a.team = bruins
  a.sport = bruins.sport
  a.started_at = 1.days.ago
  a.duration = 35
end


al_newkirk = User.create(firstname: 'Al', lastname: 'Newkirk') do |u|
  u.email = 'al.newkirk@example.com'
  u.avatar = 'https://s3-us-west-1.amazonaws.com/allstar-icons/users/alnewkirk.jpeg'
  u.height = 67
  u.weight = 182
  u.private = true
end

al_newkirk.interests.create(sport: ultimate)
al_newkirk.memberships.create(team: bruins)

al_newkirk.activities.create do |a|
  a.team = bruins
  a.sport = bruins.sport
  a.started_at = 1.days.ago
  a.duration = 42
end

al_newkirk.activities.create do |a|
  a.team = bruins
  a.sport = bruins.sport
  a.started_at = 3.days.ago
  a.duration = 60
end


kevin_cawley = User.create(firstname: 'Kevin', lastname: 'Cawley') do |u|
  u.email = 'kevin.cawley@example.com'
  u.avatar = 'https://s3-us-west-1.amazonaws.com/allstar-icons/users/kevincawley.jpeg'
  u.height = 71
  u.weight = 178
  u.private = false
end

kevin_cawley.interests.create(sport: cycling)
kevin_cawley.interests.create(sport: ultimate)
kevin_cawley.memberships.create(team: full_cycle)
kevin_cawley.memberships.create(team: bruins)

kevin_cawley.activities.create do |a|
  a.team = bruins
  a.sport = bruins.sport
  a.started_at = 1.days.ago
  a.duration = 28
end

kevin_cawley.activities.create do |a|
  a.team = full_cycle
  a.sport = full_cycle.sport
  a.started_at = 2.days.ago
  a.duration = 119
end

kevin_cawley.activities.create do |a|
  a.team = full_cycle
  a.sport = full_cycle.sport
  a.started_at = 3.days.ago
  a.duration = 223
end



