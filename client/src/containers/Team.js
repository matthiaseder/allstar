import React from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import { Link } from 'react-router-dom'
import * as profileSelectors from '../store/profiles/reducer'
import './Team.css'


class Team extends React.Component {

  render() {
    if (!this.props.teams) { return (<p>Loading...</p>) }
    const teamId = parseInt(this.props.match.params.id, 10)
    const team = this.props.teams.find(function(team) {
      return team.id === teamId
    })
    return (
      <div className="Team">
        <div className="heading">
          <div className="row">
            <div className="col-9">
              <div className="name">{team.name}</div>
              <div className="sport">
                <img src={team.sport.avatar} alt="sport avatar" />
                <span>{team.sport.name}</span>
              </div>
            </div>
            <div className="col-3 text-right">
              <img className="avatar" src={team.avatar} alt="team avatar" />
            </div>
          </div>
        </div>

        <div className="members">
          {
            team.members.map((member) =>
              <div className="row" key={member.id}>
                <div className="col-1">
                  <img className="avatar" src={member.avatar} alt="member avatar" />
                </div>
                <div className="col-3">
                  <Link to={`/users/${member.id}`}>{member.firstname} {member.lastname}</Link>
                </div>
                <div className="col-2">{member.height} inches</div>
                <div className="col-2">{member.weight} lbs</div>
                <div className="col-4">{member.email}</div>
              </div>
            )
          }
        </div>
      </div>
    )
  }

}

Team.propTypes = {
  match: PropTypes.shape({
    params: PropTypes.shape({
      id: PropTypes.string.isRequired
    })
  })
}

function mapStateToProps(state) {
  return {
    teams: profileSelectors.getTeams(state)
  }
}

export default connect(mapStateToProps)(Team)
