import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as profileActions from '../store/profiles/actions'
import * as profileSelectors from '../store/profiles/reducer'
import moment from 'moment'
import _ from 'lodash'
import './Overview.css'

const LineChart = require("react-chartjs").Line


class Overview extends Component {

  componentDidMount() {
    const organizationId = parseInt(this.props.match.params.id, 10)
    this.props.dispatch(profileActions.getOrganization(organizationId))
  }

  render() {
    if (this.isLoading()) { return (<p>Loading...</p>) }

    const chartOptions = {
      scaleShowGridLines : true,
      scaleGridLineColor : "rgba(0,0,0,.05)",
      scaleGridLineWidth : 1,
      scaleShowHorizontalLines: true,
      scaleShowVerticalLines: true,
      bezierCurve : true,
      bezierCurveTension : 0.4,
      pointDot : true,
      pointDotRadius : 4,
      pointDotStrokeWidth : 1,
      pointHitDetectionRadius : 20,
      datasetStroke : true,
      datasetStrokeWidth : 2,
      datasetFill : true,
      offsetGridLines : false
    }

    const days = _.range(6, -1, -1).map((n) => moment().subtract(n, "days").format("L"))

    const colors = [
      "151, 0, 0",
      "0, 151, 0",
      "0, 0, 151",
      "151, 187, 205",
      "151, 205, 187",
      "205, 187, 151",
      "220, 220, 220",
    ]

    const teamActivities = this.props.organization.teams.map((team, index) => {
      let activitiesDaily = []
      days.forEach((day) => activitiesDaily[day] = 0)
      for (let activity of team.activities) {
        const day = moment(activity.started_at).format('L')
        if (activitiesDaily[day] !== undefined) {
          activitiesDaily[day] += activity.duration
        }
      }

      return {
          label: team.name,
          fillColor: `rgba(${colors[index]}, 0.2)`,
          strokeColor: `rgba(${colors[index]}, 1)`,
          pointColor: `rgba(${colors[index]}, 1)`,
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: `rgba(${colors[index]}, 1)`,
          data: days.map((label) => activitiesDaily[label])
      }
    })

    const chartData = {
      labels: days,
      datasets: teamActivities
    }

    return (
      <div className="row">
        <div className="col-3">
          <div className="Overview">
           <div className="name">
              {this.props.organization.name}
            </div>
            <div className="teams mt-2">
              {
                this.props.organization.teams.map((item) =>
                  <div key={item.id} className="item">
                    <img src={item.avatar} alt={item.name} />
                    <span>{item.name}</span>
                  </div>
                 )
              }
            </div>
          </div>
        </div>
        <div className="col-9 text-center">
          <h6>Daily Activity By Team</h6>
          <LineChart data={chartData} options={chartOptions} width="800" height="500" />
        </div>
      </div>
    )
  }

  isLoading() {
    return this.props.organization.loading
  }

}

function mapStateToProps(state) {
  return {
    organization: profileSelectors.getOrganization(state)
  }
}

export default connect(mapStateToProps)(Overview)
