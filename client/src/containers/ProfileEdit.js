import React, { Component } from 'react'
import { Link, withRouter } from 'react-router-dom'
import { connect } from 'react-redux'
import * as profileActions from '../store/profiles/actions'
import * as profileSelectors from '../store/profiles/reducer'
import * as sportActions from '../store/sports/actions'
import * as sportSelectors from '../store/sports/reducer'
import ProfileForm from '../components/ProfileForm'
import './ProfileEdit.css'

class ProfileEdit extends Component {

  componentDidMount() {
    this.props.dispatch(sportActions.getAll())
    this.props.dispatch(profileActions.getProfile())
    this.handleSubmit = this.handleSubmit.bind(this)
    this.handleCancel = this.handleCancel.bind(this)
    this.handleInterestChanged = this.handleInterestChanged.bind(this)
  }

  render() {
    if (this.isLoading()) { return (<p>Loading...</p>) }
    return (
      <div className="row ProfileEdit">
        <div className="col-3">
          <img src={this.props.profile.me.avatar} className="avatar rounded" alt="avatar" />
        </div>
        <div className="col-7">
          <ProfileForm
            handleSubmit={this.handleSubmit}
            handleCancel={this.handleCancel}
            initialState={this.props.profile.me}
          />
        </div>
        <div className="col-2">
          &nbsp;
          <div className="interests">
            <div className="heading">
              Interests
            </div>
            <div className="mb-2">Select all activities you are interested in:</div>
            {
              this.props.sports.all.map(function(sport) {
                const match = this.props.profile.me.interests.find( (item) => {
                  return item.id === sport.id
                })
                return (
                  <div key={sport.id} className="item">
                    <input type="checkbox" defaultChecked={match !== undefined}
                      data-id={sport.id} onChange={this.handleInterestChanged}/>
                    <span>{sport.name}</span>
                    <img src={sport.avatar} alt={sport.name} />
                  </div>
                )
              }, this)
            }
          </div>
          <div className="mt-4 small">
            <Link to="/">Return to profile</Link>
          </div>
        </div>
      </div>
    )
  }

  handleSubmit(data) {
    const { history } = this.props
    this.props.dispatch(profileActions.setProfile(data))
    history.push("/")
  }

  handleCancel() {
    const { history } = this.props
    history.push("/")
  }

  handleInterestChanged(e) {
    const data = {
      user_id: this.props.profile.me.id,
      sport_id: e.target.getAttribute('data-id')
    }
    this.props.dispatch(profileActions.setProfileInterest(data, e.target.checked))
  }

  isLoading = () => (
    this.props.profile.loading ||
    this.props.sports.loading
  )

}

function mapStateToProps(state) {
  return {
    profile: profileSelectors.getProfile(state),
    sports: sportSelectors.getAll(state)
  }
}

export default withRouter(connect(mapStateToProps)(ProfileEdit))
