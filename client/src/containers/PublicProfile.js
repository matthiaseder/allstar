import React, { Component } from 'react'
import { connect } from 'react-redux'
import * as profileActions from '../store/profiles/actions'
import * as profileSelectors from '../store/profiles/reducer'
import moment from 'moment'
import _ from 'lodash'
import './PublicProfile.css'

const LineChart = require("react-chartjs").Line


class PublicProfile extends Component {

  componentDidMount() {
    const userId = parseInt(this.props.match.params.id, 10)
    this.props.dispatch(profileActions.getUserProfile(userId))
  }

  render() {
    if (this.isLoading()) { return (<p>Loading...</p>) }

    const chartOptions = {
      scaleShowGridLines : true,
      scaleGridLineColor : "rgba(0,0,0,.05)",
      scaleGridLineWidth : 1,
      scaleShowHorizontalLines: true,
      scaleShowVerticalLines: true,
      bezierCurve : true,
      bezierCurveTension : 0.4,
      pointDot : true,
      pointDotRadius : 4,
      pointDotStrokeWidth : 1,
      pointHitDetectionRadius : 20,
      datasetStroke : true,
      datasetStrokeWidth : 2,
      datasetFill : true,
      offsetGridLines : false
    }

    let activitiesDaily = []
    _.range(6, -1, -1).forEach((n) => activitiesDaily[moment().subtract(n, "days").format("L")] = 0)

    for (let activity of this.props.profile.data.activities) {
      const day = moment(activity.started_at).format('L')
      if (activitiesDaily[day] !== undefined) {
        activitiesDaily[day] += activity.duration
      }
    }

    const chartLabels = Object.keys(activitiesDaily)
    const chartValues = chartLabels.map((label) => activitiesDaily[label])

    const chartData = {
      labels: chartLabels,
      datasets: [
        {
          label: "Weekly Activity",
          fillColor: "rgba(151,187,205,0.2)",
          strokeColor: "rgba(151,187,205,1)",
          pointColor: "rgba(151,187,205,1)",
          pointStrokeColor: "#fff",
          pointHighlightFill: "#fff",
          pointHighlightStroke: "rgba(151,187,205,1)",
          data: chartValues
        }

      ]
    }

    return (
      <div className="row">
        <div className="col-2">
          <div className="PublicProfile">
            <div>
              <img src={this.props.profile.data.avatar} className="avatar rounded" alt="avatar" />
            </div>
            <div className="name">
              {this.props.profile.data.firstname} {this.props.profile.data.lastname}
            </div>
            <div className="email">
              {this.props.profile.data.email}
            </div>
            <div className="statistics">
              {this.displayHeight(this.props.profile.data.height)} / {this.props.profile.data.weight} lbs
            </div>
          </div>
        </div>
        <div className="col-10 text-center">
          <h6>Daily Activity Summary</h6>
          <LineChart data={chartData} options={chartOptions} width="850" height="500" />
        </div>
      </div>
    )
  }

  displayHeight(value) {
    let feet = Math.floor(value / 12)
    let inches = value % 12
    return `${feet}'${inches}"`
  }

  isLoading() {
    return this.props.profile.data.loading
  }

}

function mapStateToProps(state) {
  return {
    profile: profileSelectors.getProfile(state)
  }
}

export default connect(mapStateToProps)(PublicProfile)
