import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import { connect } from 'react-redux'
import * as profileActions from '../store/profiles/actions'
import * as profileSelectors from '../store/profiles/reducer'
import PropTypes from 'prop-types'
import './Profile.css'

class Profile extends Component {

  componentDidMount() {
    this.props.dispatch(profileActions.getProfile())
  }

  render() {
    if (this.isLoading()) { return (<p>Loading...</p>) }
    return (
      <div className="Profile">
        <div>
          <img src={this.props.profile.me.avatar} className="avatar rounded" alt="avatar" />
        </div>
        <div className="name">
          {this.props.profile.me.firstname} {this.props.profile.me.lastname}
        </div>
        <div className="email">
          {this.props.profile.me.email}
        </div>
        <div className="statistics">
          {this.displayHeight(this.props.profile.me.height)} / {this.props.profile.me.weight} lbs
        </div>

        <div className="actions">
          <div>
            <Link to="/profile/edit">Edit Profile</Link>
          </div>
          <div>
            <Link to={`/users/${this.props.profile.me.id}`}>Public Profile</Link>
          </div>
          <div>
            <Link to="/">Activity Log</Link>
          </div>
        </div>

        <div className="teams mt-4">
          <div className="heading">
            Teams
          </div>
          {
            this.props.profile.me.teams.map((item) =>
              <div key={item.id} className="item">
                <img src={item.avatar} alt={item.name} />
                <Link to={`/team/${item.id}`}>{item.name}</Link>
              </div>
             )
          }
        </div>

        <div className="interests">
          <div className="heading">
            Interests
          </div>
          {
            this.props.profile.me.interests.map((item) =>
                <div key={item.id} className="item">
                  <img src={item.avatar} alt={item.name} />
                  <span>{item.name}</span>
                </div>
              )
          }
        </div>

      </div>
    )
  }

  displayHeight(value) {
    let feet = Math.floor(value / 12)
    let inches = value % 12
    return `${feet}'${inches}"`
  }

  renderInterests() {
    return (
      <ul>
        {this.props.profile.me.interests.map(function(item) {
          return <li>item.name</li>
        })}
      </ul>
    )
  }

  isLoading() {
    return this.props.profile.me.loading
  }

}

Profile.propTypes = {
  //children: PropTypes.node,
  title: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    profile: profileSelectors.getProfile(state)
  }
}

export default connect(mapStateToProps)(Profile)
