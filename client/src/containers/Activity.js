import React, { Component } from 'react'
import PropTypes from 'prop-types'
import { connect } from 'react-redux'
import * as activityActions from '../store/activities/actions'
import * as activitySelectors from '../store/activities/reducer'
import * as profileSelectors from '../store/profiles/reducer'
import ActivityForm from '../components/ActivityForm'
import Moment from 'react-moment'
import './Activity.css'

class Activity extends Component {

  componentDidMount() {
    this.props.dispatch(activityActions.getRecent(1))
    this.handleAddActivity = this.handleAddActivity.bind(this)
  }

  render() {
    if (this.isLoading()) { return (<p>Loading...</p>) }
    return (
      <div className="Activity">
        <h6>{this.props.title}</h6>
        <div className="heading mb-4">
          <ActivityForm
            teams={this.props.profile.me.teams}
            handleAddActivity={this.handleAddActivity}
          />
        </div>

        <h6>Recent Activity</h6>
        {
          this.props.activities.recent.map( (activity) => (
            <div key={activity.id} className="row small">
              <div className="col-4">
                <Moment format="LLL">{activity.started_at}</Moment>
              </div>
              <div className="col-3">{activity.team.name}</div>
              <div className="col-3">{activity.sport.name}</div>
              <div className="col-2">{activity.duration} minutes</div>
            </div>
          ))
        }
      </div>
    )
  }

  isLoading = () => (
    this.props.profile.me.loading ||
    this.props.activities.loading
  )

  handleAddActivity(data) {
    data['user_id'] = this.props.profile.me.id
    this.props.dispatch(activityActions.createUserActivity(data))
  }

}

Activity.propTypes = {
  title: PropTypes.string.isRequired
}

function mapStateToProps(state) {
  return {
    profile: profileSelectors.getProfile(state),
    activities: activitySelectors.getRecent(state)
  }
}

export default connect(mapStateToProps)(Activity)
