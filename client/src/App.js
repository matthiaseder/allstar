import React from 'react'
import { BrowserRouter as Router, Route } from 'react-router-dom'
import { Container } from 'reactstrap'
import Profile from './containers/Profile'
import ProfileEdit from './containers/ProfileEdit'
import PublicProfile from './containers/PublicProfile'
import Activity from './containers/Activity'
import Team from './containers/Team'
import Overview from './containers/Overview'

import Footer from './components/Footer'
import Header from './components/Header'
import './App.css'

const App = () => (
  <Router>
    <div>
      <Header />
      <Container>
        <Route exact path="/" render={(props) => (
          <div className="row">
            <div className="col-4">
              <Profile {...props} title="Profile Info" /> 
            </div>
            <div className="col-8">
              <Activity {...props} title="Activity Log" /> 
            </div>
          </div>
        )}/>
        <Route exact path="/profile/edit" render={(props) => (
          <ProfileEdit {...props} title="Edit Profile" /> 
        )}/>
        <Route exact path="/team/:id" render={(props) => (
          <div className="row">
            <div className="col-4">
              <Profile {...props} title="Profile Info" /> 
            </div>
            <div className="col-8">
              <Team {...props} title="Team Info" /> 
            </div>
          </div>
        )}/>
        <Route exact path="/users/:id" render={(props) => (
          <PublicProfile {...props} /> 
        )}/>
        <Route exact path="/organizations/:id" render={(props) => (
          <Overview {...props} /> 
        )}/>
      </Container>
      <Footer />
    </div>
  </Router>
)

export default App
