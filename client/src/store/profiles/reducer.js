import Immutable from 'seamless-immutable'
import * as types from './actionTypes'

const initialState = Immutable({
  me: { loading: true },
  data: { loading: true },
  organization: { loading: true }
})

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.PROFILE_FETCHED:
      return state.merge({
        me: action.response
      })
    case types.USER_PROFILE_FETCHED:
      return state.merge({
        data: action.response
      })
    case types.ORGANIZATION_FETCHED:
      return state.merge({
        organization: action.response
      })
    case types.PROFILE_UPDATED:
      return state.merge({
        me: action.response
      })
    default:
      return state
  }
}

export function getProfile(state) {
  return state.profiles
}

export function getTeams(state) {
  return state.profiles.me.teams
}

export function getOrganization(state) {
  return state.profiles.organization
}

