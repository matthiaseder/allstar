export const PROFILE_FETCHED = "PROFILE_FETCHED"
export const PROFILE_UPDATED = "PROFILE_UPDATED"
export const PROFILE_INTEREST_ADDED = "PROFILE_INTEREST_ADDED"
export const PROFILE_INTEREST_REMOVED = "PROFILE_INTEREST_REMOVED"
export const USER_PROFILE_FETCHED = "USER_PROFILE_FETCHED"
export const ORGANIZATION_FETCHED = "ORGANIZATION_FETCHED"
