import allstarService from '../../services/allstar'
import * as types from './actionTypes'

export function getProfile() {
  return async(dispatch, getState) => {
    try {
      const response = await allstarService.getProfile()
      dispatch({ type: types.PROFILE_FETCHED, response })
    } catch (error) {
      console.error(error)
    }
  }
}

export function setProfile(data) {
  return async(dispatch, getState) => {
    try {
      const response = await allstarService.updateProfile(data)
      dispatch({ type: types.PROFILE_UPDATED, response })
    } catch (error) {
      console.error(error)
    }
  }
}

export function setProfileInterest(data, checked) {
  return async(dispatch, getState) => {
    try {
      if (checked) {
        const response = await allstarService.insertProfileInterest(data)
        dispatch({ type: types.PROFILE_INTEREST_ADDED, response })
      } else {
        const response = await allstarService.deleteProfileInterest(data)
        dispatch({ type: types.PROFILE_INTEREST_REMOVED, response })
      }
    } catch (error) {
      console.error(error)
    }
  }
}

export function getUserProfile(userId) {
  return async(dispatch, getState) => {
    try {
      const response = await allstarService.getUserProfile(userId)
      dispatch({ type: types.USER_PROFILE_FETCHED, response })
    } catch (error) {
      console.error(error)
    }
  }
}

export function getOrganization(organizationId) {
  return async(dispatch, getState) => {
    try {
      const response = await allstarService.getOrganization(organizationId)
      dispatch({ type: types.ORGANIZATION_FETCHED, response })
    } catch (error) {
      console.error(error)
    }
  }
}

