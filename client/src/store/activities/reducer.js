import Immutable from 'seamless-immutable'
import * as types from './actionTypes'

const initialState = Immutable({
  loading: false,
  recent: []
})

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.RECENT_ACTIVITIES_FETCHED:
      return state.merge({
        loading: false,
        recent: action.response
      })
    case types.USER_ACTIVITY_CREATED:
      return state.merge({
        recent: [action.response, ...state.recent]
      })
    default:
      return state
  }
}

export function getRecent(state) {
  return state.activities
}
