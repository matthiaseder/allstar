import allstarService from '../../services/allstar'
import * as types from './actionTypes'

export function getRecent(userId) {
  return async(dispatch, getState) => {
    try {
      const response = await allstarService.getRecentActivities(userId)
      dispatch({ type: types.RECENT_ACTIVITIES_FETCHED, response })
    } catch (error) {
      console.error(error)
    }
  }
}

export function createUserActivity(activity) {
  return async(dispatch, getState) => {
    try {
      const response = await allstarService.createUserActivity(activity)
      dispatch({ type: types.USER_ACTIVITY_CREATED, response })
    } catch (error) {
      console.error(error)
    }
  }
}
