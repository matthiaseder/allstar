import allstarService from '../../services/allstar'
import * as types from './actionTypes'

export function getAll() {
  return async(dispatch, getState) => {
    try {
      const response = await allstarService.getSports()
      dispatch({ type: types.SPORTS_FETCHED, response })
    } catch (error) {
      console.error(error)
    }
  }
}
