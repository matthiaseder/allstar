import Immutable from 'seamless-immutable'
import * as types from './actionTypes'

const initialState = Immutable({
  loading: true,
  all: []
})

export default function reduce(state = initialState, action = {}) {
  switch (action.type) {
    case types.SPORTS_FETCHED:
      return state.merge({
        loading: false,
        all: action.response
      })
    default:
      return state
  }
}

export function getAll(state) {
  return state.sports
}
