class AllstarService {

  async getProfile() {
    const url = '/profiles/me'
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })

    if (!response.ok) {
      throw new Error(`AllstarService getProfile failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService getProfile failed, data not returned`)
    }

    return responseData
  }

  async updateProfile(data) {
    const url = `/profiles/${data.id}`
    const response = await fetch(url, {
      method: 'PUT',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ user: data })
    })

    if (!response.ok) {
      throw new Error(`AllstarService updateProfile failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService updateProfile failed, data not returned`)
    }

    return responseData
  }

  async getSports() {
    const url = '/sports'
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })

    if (!response.ok) {
      throw new Error(`AllstarService getSports failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService getSports failed, data not returned`)
    }

    return responseData
  }


  async insertProfileInterest(data) {
    const url = '/interests'
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ interest: data })
    })

    if (!response.ok) {
      throw new Error(`AllstarService insertProfileInterest failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService insertProfileInterest failed, data not returned`)
    }

    return responseData
  }


  async deleteProfileInterest(data) {
    const url = '/interests'
    const response = await fetch(url, {
      method: 'DELETE',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ interest: data })
    })

    if (!response.ok) {
      throw new Error(`AllstarService deleteProfileInterest failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService deleteProfileInterest failed, data not returned`)
    }

    return responseData
  }

  async getRecentActivities(userId) {
    const url = `/profiles/${userId}/activities`
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })

    if (!response.ok) {
      throw new Error(`AllstarService getRecentActivities failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService getRecentActivities failed, data not returned`)
    }

    return responseData
  }

  async createUserActivity(data) {
    const url = '/activities'
    const response = await fetch(url, {
      method: 'POST',
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      },
      body: JSON.stringify({ activity: data })
    })

    if (!response.ok) {
      throw new Error(`AllstarService createUserActivity failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService createUserActivity failed, data not returned`)
    }

    return responseData
  }

  async getUserProfile(userId) {
    const url = `/profiles/${userId}`
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })

    if (!response.ok) {
      throw new Error(`AllstarService getUserProfile failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService getUserProfile failed, data not returned`)
    }

    return responseData
  }

  async getOrganization(organizationId) {
    const url = `/organizations/${organizationId}`
    const response = await fetch(url, {
      method: 'GET',
      headers: {
        Accept: 'application/json'
      }
    })

    if (!response.ok) {
      throw new Error(`AllstarService getOrganization failed, HTTP status ${response.status}`)
    }

    const responseData = await response.json()
    if (!responseData) {
      throw new Error(`AllstarService getOrganization failed, data not returned`)
    }

    return responseData
  }

}

export default new AllstarService()
