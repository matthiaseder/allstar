import React from 'react'
import { Link } from 'react-router-dom'
import { Container, Navbar, NavbarBrand, NavbarToggler, Collapse, Nav, NavItem } from 'reactstrap'
import './Header.css'

class Header extends React.Component {

  constructor(props) {
    super(props)
    this.toggle = this.toggle.bind(this)
    this.state = {
      isOpen: false
    }
  }

  toggle() {
    this.setState({
      isOpen: !this.state.isOpen
    })
  }

  render() {
    return (
      <header>
        <Navbar className="main-nav mb-4" light expand="md">
          <Container>
            <NavbarBrand href="/">Logo</NavbarBrand>
            <NavbarToggler onClick={this.toggle} />
            <Collapse isOpen={this.state.isOpen} navbar>
              <Nav className="ml-auto" navbar>
                <NavItem>
                  <Link className="nav-link" to="/">Profile</Link>
                </NavItem>
                <NavItem>
                  <Link className="nav-link" to="/organizations/1">Overview</Link>
                </NavItem>
              </Nav>
            </Collapse>
          </Container>
        </Navbar>
      </header>
    )
  }

}

export default Header
