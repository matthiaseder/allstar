import React from 'react'
import './Footer.css'

const Footer = () => (
  <footer className="mt-5 pb-3">
    <nav className="navbar navbar-expand-md navbar-fixed-bottom navbar-light" background-color="inherit">
      <div className="container">
        <span>
          &copy; 2018 UpMetrics, Inc
        </span>
            <ul className="nav navbar-nav abs-center-x">
              <li className="nav-item">
                <a className="nav-link navbar-brand" href="/">UpMetrics</a>
              </li>
        </ul>
        <ul className="nav navbar-nav">
          <li className="nav-item">
              <a className="nav-link" href="/">Terms</a>
          </li>
          <li className="nav-item">
              <a className="nav-link" href="/">Privacy</a>
          </li>
        </ul>
      </div>
    </nav>
  </footer>
)

export default Footer
