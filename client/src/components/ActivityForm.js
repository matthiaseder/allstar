import React, { Component } from 'react'
import PropTypes from 'prop-types'
import DatePicker from 'react-datepicker'
import moment from 'moment'

import "react-datepicker/dist/react-datepicker.css"
import './ActivityForm.css'


class ActivityForm extends Component {

  componentDidMount() {
    const defaultTeamId = this.props.teams.length > 0 ? this.props.teams[0].id : 0
    this.setState({
      started_at: moment(),
      team_id: defaultTeamId,
      duration: 0
    })
    this.handleStartTimeChange = this.handleStartTimeChange.bind(this)
    this.handleChange = this.handleChange.bind(this)
    this.handleAddActivity = this.handleAddActivity.bind(this)
  }

  render() {
    if (!this.state) { return (<p>Loading...</p>) }
    return (
      <form className="ActivityForm form-inline">
        <DatePicker
            selected={this.state.started_at}
            onChange={this.handleStartTimeChange}
            showTimeSelect
            timeIntervals={15}
            dateFormat="LLL"
            className="form-control form-control-sm mr-2"
        />

        <select
          id="team"
          name="team_id"
          onChange={this.handleChange}
          className="form-control form-control-sm mr-2"
        >
          {
            this.props.teams.map( (team) => (
              <option key={team.id} value={team.id}>{team.name}</option>
            ))
          }
        </select>

        <div className="input-group input-group-sm mr-2">
          <input
            name="duration"
            type="text"
            pattern="\d*"
            size={6}
            title="Integer values only please."
            onChange={this.handleChange}
            className="form-control form-control-sm"
          />
          <div className="input-group-append">
            <span className="input-group-text">minutes</span>
          </div>
        </div>

        <button
          type="submit"
          onClick={this.handleAddActivity}
          className="btn btn-sm btn-info ml-2"
        >
          Add Activity
        </button>

      </form>
    )
  }

  handleChange(e) {
    this.setState({ [e.target.name]: e.target.value })
  }

  handleStartTimeChange(moment) {
    this.setState({ started_at: moment })
  }

  handleAddActivity(e) {
    e.preventDefault()
    const { handleAddActivity } = this.props
    if (this.state.team === undefined) {

    }
    handleAddActivity(this.state)
  }

}

ActivityForm.propTypes = {
  teams: PropTypes.array.isRequired,
  handleAddActivity: PropTypes.func.isRequired
}

export default ActivityForm
