import React, { Component } from 'react'

class ProfileForm extends Component {

  constructor(props) {
    super(props)
    this.refHeightFeet = React.createRef()
    this.refHeightInches = React.createRef()
  }

  componentDidMount() {
    const { initialState } = this.props
    this.setState(initialState)
    this.onChange = this.onChange.bind(this)
    this.calculateHeight = this.calculateHeight.bind(this)
    this.onSubmit = this.onSubmit.bind(this)
    this.onCancel = this.onCancel.bind(this)
  }

  render() {
    if (!this.state) { return (<p>Loading...</p>) }
    return (
      <form onSubmit={this.onSubmit} className="ProfileForm col-10">

        <div className="form-group">
          <label htmlFor="firstname">First Name:</label>
          <input id="firstname" name="firstname" type="text" className="form-control" required
            value={this.state.firstname} onChange={this.onChange} />
        </div>
        <div className="form-group">
          <label htmlFor="lastname">Last Name:</label>
          <input id="lastname" name="lastname" type="text" className="form-control" required
            value={this.state.lastname} onChange={this.onChange} />
        </div>
        <div className="form-group">
          <label htmlFor="email">Email Address:</label>
          <input id="email" name="email" type="email" className="form-control" required
            value={this.state.email} onChange={this.onChange} />
        </div>

        <div className="row">
          <div className="col-9">
            <label>Height:</label>
          </div>
          <div className="col-3">
            <label>Weight:</label>
          </div>
        </div>


        <div className="row">
          <div className="col-4">
            <div className="input-group">
              <select
                ref={this.refHeightFeet}
                defaultValue={Math.floor(this.state.height / 12)}
                onChange={this.calculateHeight}
                className="custom-select"
              >
                {
                  [...Array(8).keys()].map(function(n) {
                    const value = n + 1;
                    return <option key={`height-${value}`} value={value}>{value}</option>
                  })
                }
              </select>
              <div className="input-group-append">
                <label className="input-group-text">feet</label>
              </div>
            </div>
          </div>

          <div className="col-5">
            <div className="input-group">
              <select
                ref={this.refHeightInches}
                defaultValue={this.state.height % 12}
                onChange={this.calculateHeight}
                className="custom-select"
              >
                {
                  [...Array(12).keys()].map(function(n) {
                    return <option key={`height-${n}`} value={n}>{n}</option>
                  })
                }
              </select>
              <div className="input-group-append">
                <label className="input-group-text">inches</label>
              </div>
            </div>

          </div>

          <div className="col-3">
            <div className="input-group">
              <input
                name="weight"
                type="text"
                pattern="\d*"
                title="Integer values only please."
                className="form-control"
                value={this.state.weight}
                onChange={this.onChange}
              />
              <div className="input-group-append">
                <span className="input-group-text">lbs</span>
              </div>
            </div>
          </div>
        </div>

        <div className="row mt-5">
          <div className="col-6">
            <button type="submit" className="btn btn-sm btn-info">
              Update profile
            </button>
            <button type="button" className="btn btn-sm btn-outline-secondary ml-3" onClick={this.onCancel}>
              Cancel
            </button>
          </div>
          <div className="col-6">
            <div className="form-check text-right">
              <input
                name="private"
                className="form-check-input"
                type="checkbox"
                checked={this.state.private}
                onChange={this.onChange}
              />
              <label className="form-check-label">
                Make profile private
              </label>
            </div>
          </div>
        </div>

      </form>
    )
  }

  onChange(e) {
    if (e.target.type === "checkbox") {
      this.setState({ [e.target.name]: e.target.checked })
    } else {
      this.setState({ [e.target.name]: e.target.value })
    }
  }

  calculateHeight() {
    const heightInInches = parseInt(this.refHeightFeet.current.value, 10) * 12 +
      parseInt(this.refHeightInches.current.value, 10)
    this.setState({ 'height': heightInInches })
  }

  onSubmit(e) {
    e.preventDefault()
    const { handleSubmit } = this.props
    handleSubmit(this.state)
  }

  onCancel(e) {
    e.preventDefault()
    const { handleCancel } = this.props
    handleCancel()
  }

}

export default ProfileForm
