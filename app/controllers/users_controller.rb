class UsersController < ApplicationController
  before_action :set_user, only: [:show, :update, :destroy]

  # GET /users/me
  # GET /users/me.json
  def index
    render partial: 'users/user', locals: { user: current_user, restricted: false }
  end

  # GET /users/1
  # GET /users/1.json
  def show
    render partial: 'users/statistics', locals: { user: @user }
  end

  # POST /users
  # POST /users.json
  def create
    @user = User.new(user_params)

    if @user.save
      render :show, status: :created, location: @user
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /users/1
  # PATCH/PUT /users/1.json
  def update
    if @user.update(user_params)
      # Always return full profile for current user. Other users' profile data may be
      # limited if it is private.
      if @user.id == current_user.id 
        render partial: 'users/user', locals: { user: @user, restricted: false }
      else
        render :show, status: :ok, location: @user
      end
    else
      render json: @user.errors, status: :unprocessable_entity
    end
  end

  # DELETE /users/1
  # DELETE /users/1.json
  def destroy
    @user.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_user
      current_user
      @user = User.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def user_params
      params.require(:user).permit(:firstname, :lastname, :email, :height, :weight, :avatar, :private)
    end
end
