class SportsController < ApplicationController
  before_action :set_sport, only: [:show, :update, :destroy]

  # GET /sports
  # GET /sports.json
  def index
    @sports = Sport.all
  end

  # GET /sports/1
  # GET /sports/1.json
  def show
  end

  # POST /sports
  # POST /sports.json
  def create
    @sport = Sport.new(sport_params)

    if @sport.save
      render :show, status: :created, location: @sport
    else
      render json: @sport.errors, status: :unprocessable_entity
    end
  end

  # PATCH/PUT /sports/1
  # PATCH/PUT /sports/1.json
  def update
    if @sport.update(sport_params)
      render :show, status: :ok, location: @sport
    else
      render json: @sport.errors, status: :unprocessable_entity
    end
  end

  # DELETE /sports/1
  # DELETE /sports/1.json
  def destroy
    @sport.destroy
  end

  private
    # Use callbacks to share common setup or constraints between actions.
    def set_sport
      @sport = Sport.find(params[:id])
    end

    # Never trust parameters from the scary internet, only allow the white list through.
    def sport_params
      params.fetch(:sport, {})
    end
end
