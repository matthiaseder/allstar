class InterestsController < ApplicationController

  # POST /interests
  # POST /interests.json
  def create
    @interest = Interest.new(interest_params)
    if (@interest.user_id != current_user.id)
      return head :forbidden
    end

    if @interest.save
      render :show, status: :created
    else
      render json: @interest.errors, status: :unprocessable_entity
    end
  end

  # DELETE /interests
  # DELETE /interests.json
  def destroy
    filtered_params = interest_params
    user_id = filtered_params[:user_id]
    sport_id = filtered_params[:sport_id]
    if (user_id != current_user.id)
      return head :forbidden
    end

    @interest = current_user.interests.find_by(sport_id: sport_id)
    @interest.destroy
    render :show, status: :ok
  end

  private
    # Never trust parameters from the scary internet, only allow the white list through.
    def interest_params
      params.require(:interest).permit(:user_id, :sport_id)
    end
end
