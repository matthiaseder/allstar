class ApplicationController < ActionController::API

  # We don't have a session, but let's pretend for demonstration purposes
  def current_user
    @current_user ||= User.first
  end

end
