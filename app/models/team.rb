class Team < ApplicationRecord
  has_many :memberships, class_name: 'TeamMember'
  has_many :members, through: :memberships, source: :user
  has_many :activities
  belongs_to :sport
  belongs_to :organization
end
