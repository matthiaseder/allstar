class Activity < ApplicationRecord
  belongs_to :user
  belongs_to :sport
  belongs_to :team
  scope :recent, -> (cutoff=1.week.ago) { where("started_at >= ?", cutoff).order("started_at desc") }
end
