class Sport < ApplicationRecord
  has_many :teams
  has_many :user_interests
  has_many :users, through: :user_interests
  has_many :activities
end
