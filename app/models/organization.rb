class Organization < ApplicationRecord
  has_many :teams
  has_many :activities, through: :teams
end
