class User < ApplicationRecord
  has_many :memberships, class_name: 'TeamMember'
  has_many :teams, through: :memberships
  has_many :interests
  has_many :sports, through: :interests
  has_many :activities
end
