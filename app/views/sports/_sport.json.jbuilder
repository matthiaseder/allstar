json.extract! sport, :id, :name, :avatar
json.url sport_url(sport, format: :json)
