json.extract! activity, :id, :user_id, :started_at, :duration
json.sport { json.extract! activity.sport, :id, :name, :avatar }
json.team { json.extract! activity.team, :id, :name, :avatar }
