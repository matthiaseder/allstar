if restricted
  json.extract! user, :id, :firstname, :lastname, :avatar, :created_at, :private
else
  json.extract! user, :id, :firstname, :lastname, :avatar, :email, :height, :weight, :created_at, :private
end
json.url user_url(user, format: :json)

json.interests do
  json.array! user.sports, partial: 'sports/sport', as: :sport
end

json.teams do
  json.array! user.teams, partial: 'teams/team', as: :team
end
