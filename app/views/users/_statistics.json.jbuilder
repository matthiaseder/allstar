if user.private
 json.extract! user, :id, :private
 json.firstname 'Private'
 json.lastname 'User'
 json.height 0
 json.weight 0
 json.avatar 'https://s3-us-west-1.amazonaws.com/allstar-icons/users/anonymous.png'
else
 json.extract! user, :id, :firstname, :lastname, :avatar, :email, :height, :weight, :created_at, :private
end

json.activities do
  json.array! user.activities.recent, partial: 'activities/activity', as: :activity
end

