restricted = @user.private
json.partial! "users/user", user: @user, restricted: restricted
