json.extract! interest, :id, :sport_id, :created_at
json.name interest.sport.name
json.avatar interest.sport.avatar
