json.extract! organization, :id, :name, :created_at
json.teams do
  json.array! organization.teams, partial: 'teams/team', as: :team
end
