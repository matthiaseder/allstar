json.extract! team, :id, :name, :avatar, :created_at
json.sport team.sport
json.members team.members.map { |m| m.slice(:id, :firstname, :lastname, :email, :height, :weight, :avatar) }
json.activities do
  json.array! team.activities.recent, partial: 'activities/activity', as: :activity
end
json.url team_url(team, format: :json)

